// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use chrono::{DateTime, Utc};
use serde::Deserialize;

/// Pipeline information.
#[derive(Debug, Deserialize, Clone)]
pub struct Pipeline {
    /// The ID of the pipeline.
    pub id: u64,
}

/// Project information.
#[derive(Debug, Deserialize, Clone)]
pub struct Project {
    /// The ID of the project.
    pub id: u64,
    /// The name of the project.
    pub name: String,
    /// The path of the project on the instance.
    pub path_with_namespace: String,
}

/// Commit information.
#[derive(Debug, Deserialize, Clone)]
pub struct Commit {
    /// The full hash of the commit.
    pub id: String,
    /// The title of the commit message.
    pub title: String,
    /// The body of the commit message.
    pub message: String,
}

/// The status of a job on a runner.
#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Eq)]
pub enum RunnerJobStatus {
    /// The job is currently running.
    #[serde(rename = "running")]
    Running,
    /// The job has completed successfully.
    #[serde(rename = "success")]
    Success,
    /// The job has completed with failure.
    #[serde(rename = "failed")]
    Failed,
    /// The job was canceled before completion.
    #[serde(rename = "canceled")]
    Canceled,
}

/// Job information.
#[derive(Debug, Deserialize, Clone)]
pub struct Job {
    /// The ID fo the job.
    pub id: u64,

    // Pipeline metadata.
    /// The name of the stage within the pipeline.
    pub stage: String,
    /// The name of the job.
    pub name: String,
    /// The pipeline the job belongs to.
    pub pipeline: Pipeline,

    // Git reference metadata.
    /// The name of the Git reference for the pipeline.
    #[serde(rename = "ref")]
    pub ref_: String,
    /// Whether the job is being built for a tag or not.
    pub tag: bool,
    /// The project which hosts the pipeline.
    pub project: Project,
    /// The commit for the pipeline.
    pub commit: Commit,

    // Runtime metadata.
    /// The status of the job.
    pub status: RunnerJobStatus,
    /// When the job was created.
    pub created_at: DateTime<Utc>,
    /// When the job was started.
    pub started_at: DateTime<Utc>,
    /// When the job finished (if completed).
    pub finished_at: Option<DateTime<Utc>>,
    /// The duration of the job (in seconds).
    pub duration: u64,
    /// How long the job waited before it was started.
    pub queued_duration: u64,
}

/// An authentication token for a runner.
#[derive(Debug, Deserialize)]
pub struct RunnerToken {
    /// The content of the token.
    pub token: String,
    /// When the token expires.
    pub token_expires_at: DateTime<Utc>,
}

/// Types of runners.
#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Eq)]
pub enum RunnerType {
    /// Instance-wide runners.
    ///
    /// May perform tasks by any project.
    #[serde(rename = "instance_type")]
    Instance,
    /// Group-specific runners.
    ///
    /// May perform jobs by projects within this or any child group.
    #[serde(rename = "group_type")]
    Group,
    /// Project-specific runners.
    ///
    /// May only perform jobs owned by the project.
    #[serde(rename = "project_type")]
    Project,
}

/// Runner access permissions.
#[derive(Debug, Deserialize, Clone, Copy, PartialEq, Eq)]
pub enum RunnerAccessLevel {
    /// Only perform jobs for pipelines associated with protected refs.
    #[serde(rename = "ref_protected")]
    RefProtected,
    /// Perform jobs for any eligible pipeline.
    #[serde(rename = "not_protected")]
    NotProtected,
}

/// Overview of a runner.
#[derive(Debug, Deserialize)]
pub struct RunnerOverview {
    /// The ID of the runner.
    pub id: u64,

    // Metadata.
    /// The name of the runner.
    pub name: Option<String>,
    /// The description of the runner.
    pub description: Option<String>,

    // Registration information.
    /// Whether the runner is shared or not.
    pub is_shared: bool,
    /// The type of the runner.
    pub runner_type: RunnerType,

    // State information.
    /// Whether the runner is paused or not.
    pub paused: bool,
    /// Whether the runner is online or not.
    pub online: Option<bool>,
    /// The IP address of the runner.
    pub ip_address: Option<String>, // TODO: parse into an address?
}

impl RunnerOverview {
    /// Upgrade an overview into a (partial) `RunnerDetails` structure.
    ///
    /// Fields will be incomplete, so only do this if the fields not present in `RunnerOverview`
    /// are not required.
    pub fn upgrade(self) -> RunnerDetails {
        RunnerDetails {
            id: self.id,
            name: self.name,
            description: self.description,
            platform: None,
            architecture: None,
            revision: None,
            is_shared: self.is_shared,
            runner_type: self.runner_type,
            tags: Vec::new(),
            paused: self.paused,
            online: self.online,
            ip_address: self.ip_address,
            contacted_at: None,
            access_level: RunnerAccessLevel::RefProtected,
            maximum_timeout: None,
        }
    }
}

/// Details on a runner.
#[derive(Debug, Deserialize)]
pub struct RunnerDetails {
    /// The ID of the runner.
    pub id: u64,

    // Metadata.
    /// The name of the runner.
    pub name: Option<String>,
    /// The description of the runner.
    pub description: Option<String>,
    /// The platform of the runner.
    pub platform: Option<String>,
    /// The architecture of the runner.
    pub architecture: Option<String>,
    /// The revision of the runner.
    pub revision: Option<String>,

    // Registration information.
    /// Whether the runner is shared or not.
    pub is_shared: bool,
    /// The type of the runner.
    pub runner_type: RunnerType,
    /// The set of tags for the runner.
    #[serde(default)]
    pub tags: Vec<String>,

    // State information.
    /// Whether the runner is paused or not.
    pub paused: bool,
    /// Whether the runner is online or not.
    pub online: Option<bool>,
    /// The IP address of the runner.
    pub ip_address: Option<String>, // TODO: parse into an address?
    /// When the runner last contacted the GitLab instance.
    pub contacted_at: Option<DateTime<Utc>>,

    // Settings.
    /// The access level to the runner.
    pub access_level: RunnerAccessLevel,
    /// The maximum timeout allowed for the runner.
    pub maximum_timeout: Option<u64>,
}
